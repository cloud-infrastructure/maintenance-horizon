(function() {
    'use strict';

    angular
	.module('maintenanceapp')
	.controller('MainController', MainController);

    MainController.$inject = ['$scope', 'rss.service'];


    function MainController($scope, rssService){

        $scope.message = {}
        init();

	function init(){
            rssService.getMessage().then(function(d) {
                $scope.message = d.data;
            });
	}
    }

})();
