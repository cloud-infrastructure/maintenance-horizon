(function () {
  'use strict';

  angular
    .module('maintenanceapp')
    .factory('rss.service', rssService);

  rssService.$inject = [
    '$http'
  ];

  function rssService($http) {
    var service = {
      getMessage: getMessage,
    };

    return service;

    function getMessage(){
      var url = 'message.json';
      var promise = $http.get(url).success(function(data){
          return data;
      });
      return promise;
    }
  }
})();
