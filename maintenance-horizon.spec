Name:           maintenance-horizon
Version:        0.2
Release:        5%{?dist}
Summary:        Web application to replace OpenStack Dashboard during interventions

License:        ASL 2.0
URL:            https://gitlab.cern.ch/cloud-infrastructure/maintenance-horizon
Source0:        %{name}-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  systemd

Requires(post): systemd
Requires:       httpd

%description
Web application to replace OpenStack Dashboard during interventions

%prep
%autosetup -p1 -n %{name}-%{version}

%build

%install
mkdir -p %{buildroot}/var/www/html
cp -r %{_builddir}/%{name}-%{version}/*  %{buildroot}/var/www/html/

%files
%doc LICENSE
%doc README.md
%attr(0644, apache, apache) /var/www/html/*
%attr(0755, apache, apache) /var/www/html/assets
%attr(0755, apache, apache) /var/www/html/assets/default_100_percent
%attr(0755, apache, apache) /var/www/html/assets/default_200_percent

%post
%systemd_post httpd.service

%preun
%systemd_preun httpd.service

%postun
%systemd_postun_with_restart httpd.service

%changelog
* Fri Sep 01 2023 Jose Castro Leon <jose.castro.leon@cern.ch> 0.2-5
- Rebuild for RHEL and ALMA

* Thu Nov 03 2022 Domingo Rivera Barros <driverab@cern.ch> 0.2-4
- Update OTG URL path

* Tue Apr 13 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 0.2-3
- Initial rebuild for el8s

* Fri Jun 05 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 0.2-2
- Remove piwik tracking code

* Fri Jun 05 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 0.2-1
- Repackaged to be able to build it simultaneously on centos 7 and 8

* Tue Oct 24 2017 Mateusz Kowalski <mateusz.kowalski@cern.ch> 0.1-4
- Added Chromium Dino Runner from https://github.com/wayou/t-rex-runner

* Thu Feb 11 2016 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 0.1-2
- Removed console.log
- Added LICENSE and AUTHORS files

* Fri Jan 29 2016 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 0.1-1
- First RPM
